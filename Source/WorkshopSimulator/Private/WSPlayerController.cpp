// Fill out your copyright notice in the Description page of Project Settings.


#include "WSPlayerController.h"

void AWSPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

bool AWSPlayerController::AddInventoryItem(UWSItemBase * NewItem, int32 ItemCount, int32 ItemLevel, bool bAutoSlot)
{
	bool bChanged = false;
	if (!NewItem)
	{
		UE_LOG(LogWS, Warning, TEXT("AddInventoryItem: Failed trying to add null item!"))
	}
	if (ItemCount <= 0 || ItemLevel <= 0)
	{
		UE_LOG(LogWS, Warning, TEXT("AddInventoryItem: Failed trying to add item %s with negative count or level!"), *NewItem->GetName());
		return false;
	}

	// Retrieve current item data if it is in inventory
	FWSItemData OldData;
	GetInventoryItemData(NewItem, OldData);

	FWSItemData NewData = OldData;
	NewData.UpdateItemData(FWSItemData(ItemCount, ItemLevel), NewItem->MaxCount, NewItem->MaxLevel);

	if (OldData != NewData)
	{
		InventoryData.Add(NewItem, NewData);
		//NotifyInventoryItemChanged(true, NewItem);
		bChanged = true;
	}

	if (bAutoSlot)
	{
		//// Slot item if required
		//bChanged |= FillEmptySlotWithItem(NewItem);
	}

	if (bChanged)
	{
		// If anything changed, write to save game
		SaveInventory();
		return true;
	}
	return false;
}


bool AWSPlayerController::RemoveInventoryItem(UWSItemBase * RemovedItem, int32 RemoveCount)
{
	if (!RemovedItem)
	{
		UE_LOG(LogWS, Warning, TEXT("RemoveInventoryItem: Failed trying to remove null item!"));
		return false;
	}

	FWSItemData NewData;
	GetInventoryItemData(RemovedItem, NewData);

	if (!NewData.IsValid())
	{
		//Not found
		return false;
	}

	//Remove all if <= 0
	if (RemoveCount <= 0)
	{
		NewData.ItemCount = 0;
	}
	else
	{
		NewData.ItemCount -= RemoveCount;
	}

	if (NewData.ItemCount > 0)
	{
		InventoryData.Add(RemovedItem, NewData);
	}
	else 
	{
		InventoryData.Remove(RemovedItem);
	}
	
	SaveInventory();
	return true;
}

void AWSPlayerController::GetInventoryItems(TArray<UWSItemBase*>& Items, FPrimaryAssetType ItemType)
{
	for (const TPair<UWSItemBase*, FWSItemData>& Pair : InventoryData)
	{
		if (Pair.Key)
		{
			FPrimaryAssetId AssetId = Pair.Key->GetPrimaryAssetId();

			// Filters based on item type
			if (AssetId.PrimaryAssetType == ItemType || !ItemType.IsValid())
			{
				Items.Add(Pair.Key);
			}
		}
	}
}

int32 AWSPlayerController::GetInventoryItemCount(UWSItemBase * Item) const
{
	const FWSItemData* FoundItem = InventoryData.Find(Item);

	if (FoundItem)
	{
		return FoundItem->ItemCount;
	}
	return 0;
}

bool AWSPlayerController::GetInventoryItemData(UWSItemBase * Item, FWSItemData & ItemData) const
{
	const FWSItemData* FoundItem = InventoryData.Find(Item);

	if (FoundItem)
	{
		ItemData = *FoundItem;
		return true;
	}
	ItemData = FWSItemData(0, 0);
	return false;
}

void AWSPlayerController::GetSlottedItems(TArray<UWSItemBase*>& Items, FPrimaryAssetType ItemType, bool bOutputEmptyIndexes)
{
}

void AWSPlayerController::FillEmptySlots()
{
}

bool AWSPlayerController::SaveInventory()
{
	//UWorld* World = GetWorld();
	//URPGGameInstanceBase* GameInstance = World ? World->GetGameInstance<URPGGameInstanceBase>() : nullptr;

	//if (!GameInstance)
	//{
	//	return false;
	//}

	//URPGSaveGame* CurrentSaveGame = GameInstance->GetCurrentSaveGame();
	//if (CurrentSaveGame)
	//{
	//	// Reset cached data in save game before writing to it
	//	CurrentSaveGame->InventoryData.Reset();
	//	CurrentSaveGame->SlottedItems.Reset();

	//	for (const TPair<URPGItem*, FRPGItemData>& ItemPair : InventoryData)
	//	{
	//		FPrimaryAssetId AssetId;

	//		if (ItemPair.Key)
	//		{
	//			AssetId = ItemPair.Key->GetPrimaryAssetId();
	//			CurrentSaveGame->InventoryData.Add(AssetId, ItemPair.Value);
	//		}
	//	}

	//	for (const TPair<FRPGItemSlot, URPGItem*>& SlotPair : SlottedItems)
	//	{
	//		FPrimaryAssetId AssetId;

	//		if (SlotPair.Value)
	//		{
	//			AssetId = SlotPair.Value->GetPrimaryAssetId();
	//		}
	//		CurrentSaveGame->SlottedItems.Add(SlotPair.Key, AssetId);
	//	}

	//	// Now that cache is updated, write to disk
	//	GameInstance->WriteSaveGame();
	//	return true;
	//}
	return false;
}

bool AWSPlayerController::LoadInventory()
{
	//InventoryData.Reset();
	//SlottedItems.Reset();

	//// Fill in slots from game instance
	//UWorld* World = GetWorld();
	//URPGGameInstanceBase* GameInstance = World ? World->GetGameInstance<URPGGameInstanceBase>() : nullptr;

	//if (!GameInstance)
	//{
	//	return false;
	//}

	//for (const TPair<FPrimaryAssetType, int32>& Pair : GameInstance->ItemSlotsPerType)
	//{
	//	for (int32 SlotNumber = 0; SlotNumber < Pair.Value; SlotNumber++)
	//	{
	//		SlottedItems.Add(FRPGItemSlot(Pair.Key, SlotNumber), nullptr);
	//	}
	//}

	//URPGSaveGame* CurrentSaveGame = GameInstance->GetCurrentSaveGame();
	//URPGAssetManager& AssetManager = URPGAssetManager::Get();
	//if (CurrentSaveGame)
	//{
	//	// Copy from save game into controller data
	//	bool bFoundAnySlots = false;
	//	for (const TPair<FPrimaryAssetId, FRPGItemData>& ItemPair : CurrentSaveGame->InventoryData)
	//	{
	//		URPGItem* LoadedItem = AssetManager.ForceLoadItem(ItemPair.Key);

	//		if (LoadedItem != nullptr)
	//		{
	//			InventoryData.Add(LoadedItem, ItemPair.Value);
	//		}
	//	}

	//	for (const TPair<FRPGItemSlot, FPrimaryAssetId>& SlotPair : CurrentSaveGame->SlottedItems)
	//	{
	//		if (SlotPair.Value.IsValid())
	//		{
	//			URPGItem* LoadedItem = AssetManager.ForceLoadItem(SlotPair.Value);
	//			if (GameInstance->IsValidItemSlot(SlotPair.Key) && LoadedItem)
	//			{
	//				SlottedItems.Add(SlotPair.Key, LoadedItem);
	//				bFoundAnySlots = true;
	//			}
	//		}
	//	}

	//	if (!bFoundAnySlots)
	//	{
	//		// Auto slot items as no slots were saved
	//		FillEmptySlots();
	//	}

	//	NotifyInventoryLoaded();

	//	return true;
	//}

	//// Load failed but we reset inventory, so need to notify UI
	//NotifyInventoryLoaded();

	return false;
}
