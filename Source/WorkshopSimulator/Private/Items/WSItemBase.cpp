// Fill out your copyright notice in the Description page of Project Settings.


#include "Items\WSItemBase.h"

FString UWSItemBase::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId UWSItemBase::GetPrimaryAssetId() const
{
	return FPrimaryAssetId();
}
