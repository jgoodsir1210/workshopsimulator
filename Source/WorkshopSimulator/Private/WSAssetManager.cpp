// Fill out your copyright notice in the Description page of Project Settings.


#include "WSAssetManager.h"
#include "WorkshopSimulator.h"
#include "Items/WSItemBase.h"
#include "AbilitySystemGlobals.h"

const FPrimaryAssetType	UWSAssetManager::WeaponItemType = TEXT("Weapons");
const FPrimaryAssetType	UWSAssetManager::ArmourItemType = TEXT("Armour");
const FPrimaryAssetType	UWSAssetManager::ResourceItemType = TEXT("Resources");
const FPrimaryAssetType	UWSAssetManager::KeyItemType = TEXT("Key");
const FPrimaryAssetType	UWSAssetManager::FoodItemType = TEXT("Food");

UWSAssetManager& UWSAssetManager::Get()
{
	UWSAssetManager* This = Cast<UWSAssetManager>(GEngine->AssetManager);

	if (This)
	{
		return *This;
	}
	else {
		UE_LOG(LogWS, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be WSAssetManager!"));
		return *NewObject<UWSAssetManager>(); // never calls this
	}
}

void UWSAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	UAbilitySystemGlobals::Get().InitGlobalData();
}

UWSItemBase* UWSAssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	UWSItemBase* LoadedItem = Cast<UWSItemBase>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr)
	{
		UE_LOG(LogWS, Warning, TEXT("Failed to load item for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedItem;
}