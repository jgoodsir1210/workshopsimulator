// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "Engine/Engine.h"
#include "Net/UnrealNetwork.h"

WORKSHOPSIMULATOR_API DECLARE_LOG_CATEGORY_EXTERN(LogWS, Log, All);