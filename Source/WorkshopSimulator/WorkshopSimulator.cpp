// Fill out your copyright notice in the Description page of Project Settings.

#include "WorkshopSimulator.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WorkshopSimulator, "WorkshopSimulator" );

DEFINE_LOG_CATEGORY(LogWS);