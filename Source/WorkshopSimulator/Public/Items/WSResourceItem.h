// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/WSItemBase.h"
#include "WSResourceItem.generated.h"

/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API UWSResourceItem : public UWSItemBase
{
	GENERATED_BODY()

public:
	UWSResourceItem()
	{
		ItemType = UWSAssetManager::ResourceItemType;
	}
};
