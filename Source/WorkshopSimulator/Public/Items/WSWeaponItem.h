// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/WSItemBase.h"
#include "WSWeaponItem.generated.h"

/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API UWSWeaponItem : public UWSItemBase
{
	GENERATED_BODY()

public:
	UWSWeaponItem()
	{
		ItemType = UWSAssetManager::WeaponItemType;
	}

	/** Weapon actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
		TSubclassOf<AActor> WeaponActor;
};
