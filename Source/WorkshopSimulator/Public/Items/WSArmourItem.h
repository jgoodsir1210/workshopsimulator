// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/WSItemBase.h"
#include "WSArmourItem.generated.h"

/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API UWSArmourItem : public UWSItemBase
{
	GENERATED_BODY()
	
public:
	UWSArmourItem()
	{
		ItemType = UWSAssetManager::ArmourItemType;
	}
};
