// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/WSItemBase.h"
#include "WSFoodItem.generated.h"

/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API UWSFoodItem : public UWSItemBase
{
	GENERATED_BODY()

public:
	UWSFoodItem()
	{
		ItemType = UWSAssetManager::FoodItemType;
	}
};
