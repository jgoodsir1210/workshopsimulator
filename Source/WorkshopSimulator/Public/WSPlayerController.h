// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Items/WSItemBase.h"
#include "WSPlayerController.generated.h"

class UWSItemBase;
/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API AWSPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AWSPlayerController() {}
	virtual void BeginPlay() override;

	/** Map of all items owned by this player, from definition to data */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
		TMap<UWSItemBase*, FWSItemData> InventoryData;

	/** Adds a new inventory item, will add it to an empty slot if possible. If the item supports count you can add more than one count. It will also update the level when adding if required */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool AddInventoryItem(UWSItemBase* NewItem, int32 ItemCount = 1, int32 ItemLevel = 1, bool bAutoSlot = true);

	/** Remove an inventory item, will also remove from slots. A remove count of <= 0 means to remove all copies */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool RemoveInventoryItem(UWSItemBase* RemovedItem, int32 RemoveCount = 1);

	/** Returns all inventory items of a given type. If none is passed as type it will return all */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		void GetInventoryItems(TArray<UWSItemBase*>& Items, FPrimaryAssetType ItemType);

	/** Returns number of instances of this item found in the inventory. This uses count from GetItemData */
	UFUNCTION(BlueprintPure, Category = Inventory)
		int32 GetInventoryItemCount(UWSItemBase* Item) const;

	/** Returns the item data associated with an item. Returns false if none found */
	UFUNCTION(BlueprintPure, Category = Inventory)
		bool GetInventoryItemData(UWSItemBase* Item, FWSItemData& ItemData) const;

	/** Returns all slotted items of a given type. If none is passed as type it will return all */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		void GetSlottedItems(TArray<UWSItemBase*>& Items, FPrimaryAssetType ItemType, bool bOutputEmptyIndexes);

	/** Fills in any empty slots with items in inventory */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		void FillEmptySlots();

	/** Manually save the inventory, this is called from add/remove functions automatically */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool SaveInventory();

	/** Loads inventory from save game on game instance, this will replace arrays */
	UFUNCTION(BlueprintCallable, Category = Inventory)
		bool LoadInventory();

	///** Delegate called when an inventory item has been added or removed */
	//UPROPERTY(BlueprintAssignable, Category = Inventory)
	//	FOnInventoryItemChanged OnInventoryItemChanged;

	///** Called after the inventory was changed and we notified all delegates */
	//UFUNCTION(BlueprintImplementableEvent, Category = Inventory)
	//	void InventoryItemChanged(bool bAdded, UWSItemBase* Item);

	///** Delegate called when the inventory has been loaded/reloaded */
	//UPROPERTY(BlueprintAssignable, Category = Inventory)
	//	FOnInventoryLoaded OnInventoryLoaded;

	///** Map of slot, from type/num to item, initialized from ItemSlotsPerType on RPGGameInstanceBase */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	//	TMap<FRPGItemSlot, UWSItemBase*> SlottedItems;

	///** Native version above, called before BP delegate */
	//FOnInventoryItemChangedNative OnInventoryItemChangedNative;

	///** Delegate called when an inventory slot has changed */
	//UPROPERTY(BlueprintAssignable, Category = Inventory)
	//	FOnSlottedItemChanged OnSlottedItemChanged;

	///** Called after an item was equipped and we notified all delegates */
	//UFUNCTION(BlueprintImplementableEvent, Category = Inventory)
	//	void SlottedItemChanged(FRPGItemSlot ItemSlot, UWSItemBase* Item);

	///** Native version above, called before BP delegate */
	//FOnSlottedItemChangedNative OnSlottedItemChangedNative;

	///** Native version above, called before BP delegate */
	//FOnInventoryLoadedNative OnInventoryLoadedNative;

	///** Sets slot to item, will remove from other slots if necessary. If passing null this will empty the slot */
	//UFUNCTION(BlueprintCallable, Category = Inventory)
	//	bool SetSlottedItem(FRPGItemSlot ItemSlot, UWSItemBase* Item);

	///** Returns item in slot, or null if empty */
	//UFUNCTION(BlueprintPure, Category = Inventory)
	//	UWSItemBase* GetSlottedItem(FRPGItemSlot ItemSlot) const;

};
