// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WorkshopSimulator.h"
#include "Engine/AssetManager.h"
#include "WSAssetManager.generated.h"

class UWSItemBase;

/**
 * 
 */
UCLASS()
class WORKSHOPSIMULATOR_API UWSAssetManager : public UAssetManager
{
	GENERATED_BODY()
	
public:
	UWSAssetManager() {}
	virtual void StartInitialLoading() override;

	/** Static types for items */

	static const FPrimaryAssetType WeaponItemType;
	static const FPrimaryAssetType ArmourItemType;
	static const FPrimaryAssetType ResourceItemType;
	static const FPrimaryAssetType FoodItemType;
	static const FPrimaryAssetType KeyItemType;

	static UWSAssetManager& Get();

	UWSItemBase* ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);

};
